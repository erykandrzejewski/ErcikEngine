#include <Ercik/Server.hpp>
#include <algorithm>

namespace ee
{
  Server::Server(const std::string& server_address, const int server_port)
  {
    setAddress(server_address);
    setPort(server_port);
  }

  void Server::setAddress(const std::string& server_address)
  {
    this->server_address = server_address;
  }

  void Server::setPort(const int server_port)
  {
    this->server_port = server_port;
  }

  std::string Server::getAddress()
  {
    return server_address;
  }

  int Server::getPort()
  {
    return server_port;
  }

  void Server::addChannel(const std::string& channel)
  {
    auto position = std::find(channels_list.begin(), channels_list.end(), channel);

    if (position == channels_list.end())
      channels_list.push_back(channel);
  }

  void Server::removeChannel(const std::string& channel)
  {
    auto position = std::find(channels_list.begin(), channels_list.end(), channel);

    if (position != channels_list.end())
      channels_list.erase(position);
  }

  std::vector<std::string> Server::getChannelsList()
  {
    return channels_list;
  }

  bool Server::operator==(const ee::Server& server)
  {
      return (server_address == server.server_address) && (server_port == server.server_port);
  }
}
