#include <Ercik/Bot.hpp>
#include <Ercik/IrcSocketException.hpp>
#include <Ercik/IrcSocketSfml.hpp>
#include <Ercik/IrcMessage.hpp>
#include <Ercik/IrcMessageException.hpp>

#include <algorithm>
#include <fstream>
#include <sstream>
#include <iostream>
#include <string>
#include <thread>
#include <chrono>

namespace ee
{
	Bot::Bot(const std::string& nickname)
	{
    setNickname(nickname);
	}

	void Bot::setNickname(const std::string& nickname)
	{
		this->nickname = nickname;
	}

	void Bot::addServer(ee::Server& server)
	{
		auto position = std::find(servers_list.begin(), servers_list.end(), server);

		if (position == servers_list.end())
			servers_list.push_back(server);
	}

	void Bot::removeServer(ee::Server& server)
	{
		auto position = std::find(servers_list.begin(), servers_list.end(), server);

		if (position != servers_list.end())
				servers_list.erase(position);
	}

	void Bot::addPlugin(ee::Plugin& plugin)
	{
		plugins_list.push_back(plugin);
	}

	void Bot::initialize()
	{
		for (ee::Server server : servers_list)
		{
      try
      {
        std::unique_ptr<ee::IrcSocket> socket(new ee::IrcSocketSfml);
    		socket->connect(server.getAddress(), server.getPort());
      
			  socket->sendData("NICK " + nickname);
			  socket->sendData("USER " + nickname + " " + nickname + " " + nickname + " :" + nickname);

			  for (std::string channel : server.getChannelsList())
			  {
			  	socket->sendData("JOIN " + channel);
			  }

        sockets_list.push_back(std::move(socket));
      }
      catch (ee::IrcSocketConnectException& exc)
      {
        std::cerr << exc.what() << '\n';
      }
      catch (ee::IrcSocketDataSendException& exc)
      {
        std::cerr << exc.what() << '\n';
      }
		}
	}

	void Bot::run()
	{
		initialize();

    while (true)
    {
      for (unsigned i = 0; i < sockets_list.size(); ++i)
      {
        try
        {
          std::string data = sockets_list[i]->receiveData();
          if (data.size() > 0)
          {
            std::cout << "[SERVER " << i << "] " << data;
          }
        }
        catch (ee::IrcSocketException& e)
        {
          std::cerr << e.what() << '\n';
          sockets_list[i]->disconnect();
          sockets_list.erase(sockets_list.begin() + i);
        }
      }

            
      // Plugins calling
    }
	}
}
