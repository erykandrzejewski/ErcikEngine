#include <Ercik/IrcMessage.hpp>
#include <Ercik/IrcMessageException.hpp>
#include <sstream>
#include <string>
#include <iostream>

namespace ee
{
  IrcMessage::IrcMessage() :
  sender_type(ee::IrcMessage::SenderType::None)
  {
  }

  void IrcMessage::setServerMessage(const std::string& server_raw_message)
  {
    if (sender_type == ee::IrcMessage::SenderType::Client)
      throw ee::IrcMessageException("This is client message!");

    sender_type = ee::IrcMessage::SenderType::Server;

    // Parsing the message...

    std::istringstream message_iss(server_raw_message);
    std::string message_token;

    std::getline(message_iss, message_token, ' ');

    is_server_the_sender = false;

    if (message_token[0] == ':')
    {
      // There is a prefix in the message;
      has_prefix = true;
      message_token.erase(0, 1);

      // Parsing the prefix
      /* Prefix types
       * 1) prefix = server_name
       * 2) prefix = nickname
       * 3) prefix = nickname@hostname
       * 4) prefix = nickname!username@hostname
       */

      if (message_token.find("@") == std::string::npos)
      {
       if (message_token.find(".") != std::string::npos)
       {
         // 1
         server_name = message_token;
         is_server_the_sender = true;
       }
       else
       {
         // 2
         sender_nickname = message_token;
       }
      }
      else
      {
        std::istringstream prefix_iss(message_token);
        std::string prefix_token;

        if (message_token.find("!") == std::string::npos)
        {
          // 3
          std::getline(prefix_iss, sender_nickname, '@');
          std::getline(prefix_iss, sender_hostname);
        }
        else
        {
          // 4
          std::getline(prefix_iss, sender_nickname, '!');
          std::getline(prefix_iss, sender_username, '@');
          std::getline(prefix_iss, sender_hostname);
        }
      }

      std::getline(message_iss, command, ' ');
    }
    else
    {
      has_prefix = false;
      command = message_token;
    }

    std::getline(message_iss, message_token, ':');

    std::istringstream parameters_iss(message_token);
    std::string parameter;

    while (std::getline(parameters_iss, parameter, ' '))
      command_parameters_list.push_back(parameter);

    message_token = "";

    std::getline(message_iss, message_token);

    if (message_token != "")
    {
      if (message_token[message_token.size() - 1] == '\r')
        message_token.erase(message_token.size() - 1, 1);

      command_parameters_list.push_back(message_token);
    }
  }

  bool IrcMessage::hasPrefix()
  {
    if (sender_type != ee::IrcMessage::SenderType::Server)
      throw ee::IrcMessageException("This is not server message!");

    return has_prefix;
  }

  bool IrcMessage::isServerTheSender()
  {
    if (sender_type != ee::IrcMessage::SenderType::Server)
      throw ee::IrcMessageException("This is not server message!");
    if (!has_prefix)
      throw ee::IrcMessageException("The message hasn't prefix!");

    return is_server_the_sender;
  }

  std::string IrcMessage::getServerName()
  {
    if (sender_type != ee::IrcMessage::SenderType::Server)
      throw ee::IrcMessageException("This is not server message!");
    if (!has_prefix)
      throw ee::IrcMessageException("The message hasn't prefix!");
    if (!is_server_the_sender)
      throw ee::IrcMessageException("The message hasn't been sent by server!");

    return server_name;
  }

  std::string IrcMessage::getSenderNickname()
  {
    if (sender_type != ee::IrcMessage::SenderType::Server)
      throw ee::IrcMessageException("This is not server message!");
    if (!has_prefix)
      throw ee::IrcMessageException("The message hasn't prefix!");
    if (is_server_the_sender)
      throw ee::IrcMessageException("The message has been sent by server!");

    return sender_nickname;
  }

  std::string IrcMessage::getSenderUsername()
  {
    if (sender_type != ee::IrcMessage::SenderType::Server)
      throw ee::IrcMessageException("This is not server message!");
    if (!has_prefix)
      throw ee::IrcMessageException("The message hasn't prefix!");
    if (is_server_the_sender)
      throw ee::IrcMessageException("The message has been sent by server!");

    return sender_username;
  }

  std::string IrcMessage::getSenderHostname()
  {
    if (sender_type != ee::IrcMessage::SenderType::Server)
      throw ee::IrcMessageException("This is not server message!");
    if (!has_prefix)
      throw ee::IrcMessageException("The message hasn't prefix!");
    if (is_server_the_sender)
      throw ee::IrcMessageException("The message has been sent by server!");

    return sender_hostname;
  }

  std::string IrcMessage::getCommand()
  {
    if (sender_type == ee::IrcMessage::SenderType::None)
      throw ee::IrcMessageException("The message is empty!");

    return command;
  }

  std::vector<std::string> IrcMessage::getCommandParametersList()
  {
    if (sender_type == ee::IrcMessage::SenderType::None)
      throw ee::IrcMessageException("The message is empty!");

    return command_parameters_list;
  }

  std::string IrcMessage::getCommandParameter(int index)
  {
    // There isn't neccessary to check sender type and throw an exception
    return getCommandParametersList()[index];
  }

  std::string IrcMessage::getClientMessage()
  {
    if (sender_type != ee::IrcMessage::SenderType::Client)
      throw ee::IrcMessageException("This is not client message!");

    std::string client_raw_message = command;

    for (int i = 0; i < command_parameters_list.size(); ++i)
    {
      if (i == command_parameters_list.size() - 1 && command_parameters_list[i].find(" ") != std::string::npos)
        client_raw_message += (" :" + command_parameters_list[i]);
      else
        client_raw_message += (' ' + command_parameters_list[i]);
    }
    return client_raw_message;
  }

  void IrcMessage::setCommand(const std::string& command)
  {
    if (sender_type == ee::IrcMessage::SenderType::Server)
      throw ee::IrcMessageException("This is server message!");
    sender_type = ee::IrcMessage::SenderType::Client;

    this->command = command;
  }

  void IrcMessage::addCommandParameter(const std::string& parameter)
  {
    if (sender_type == ee::IrcMessage::SenderType::Server)
      throw ee::IrcMessageException("This is server message!");
    sender_type = ee::IrcMessage::SenderType::Client;

    command_parameters_list.push_back(parameter);
  }
}
