#ifndef IRC_SOCKET_EXCEPTION_HPP
#define IRC_SOCKET_EXCEPTION_HPP

#include <stdexcept>
#include <string>

namespace ee
{
  class IrcSocketException : public std::runtime_error
  {
  public:
    IrcSocketException(const std::string& message) :
    std::runtime_error(message)
    {
    }
  };



  class IrcSocketConnectException : public IrcSocketException
  {
  public:
    IrcSocketConnectException(const std::string& message) :
    IrcSocketException(message)
    {
    }
  };

  class IrcSocketDataSendException : public IrcSocketException
  {
  public:
    IrcSocketDataSendException(const std::string& message) :
    IrcSocketException(message)
    {
    } 
  };

  class IrcSocketDataReceiveException : public IrcSocketException
  {
  public:
    IrcSocketDataReceiveException(const std::string& message) :
    IrcSocketException(message)
    {
    }
  };
}

#endif
